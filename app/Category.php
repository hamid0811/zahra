<?php

namespace Zahra;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function projects(){
        return $this->belongsToMany('Zahra\Project');
    }

    public function categories(){
        return $this->belongsToMany('Zahra\Project');
    }

    public function number_of_projects(){
        return $this->belongsToMany('Zahra\Project');
    }
}
