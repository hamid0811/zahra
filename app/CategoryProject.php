<?php

namespace Zahra;

use Illuminate\Database\Eloquent\Model;

class CategoryProject extends Model
{
    public $table = "category_project";
}
