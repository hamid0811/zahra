<?php

namespace Zahra\Http\Controllers;

use Zahra\Article;
use Zahra\Http\Resources\Article as ArticlesResource;
use Illuminate\Http\Request;
use Zahra\Http\Requests;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get Artitles
        $articles = Article::paginate(5);

        return view ('index', compact('articles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $article = $request->isMethod('put') ? Article::findOrFail ($request->article_id) : new Article;

        $article->id = $request->input('article_id');
        $article->title = $request->input('title');
        $article->body = $request->input('body');

        if($article->save()){
            return new ArticlesResource($article);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Get article
        $article = Article::findOrFail($id);

        //Return sinle article as a resource
        return new ArticlesResource($article);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Get article
        $article = Article::findOrFail($id);

        if($article->delete()){
            return new ArticlesResource($article);
        }

    }
}
