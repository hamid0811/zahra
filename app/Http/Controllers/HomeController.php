<?php

namespace Zahra\Http\Controllers;

use Illuminate\Http\Request;
use Zahra\Category;
use Zahra\Profile;
use Zahra\Project;
use Zahra\Setting;

class HomeController extends Controller
{


    /**
     * Show the application dashboard.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Project $project)
    {
        $categories = Category::get();
        $projects = Project::with('images','categories')->where(['is_featured' => '1', 'is_published' => '1'])->OrderBy('id', 'desc')->get();
        $contact = Profile::first();
        $settings = Setting::first();
        return view('index', compact('projects', 'categories', 'contact', 'settings'));
    }
}
