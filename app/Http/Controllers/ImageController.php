<?php

namespace Zahra\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as InterventionImage;
use Zahra\Image;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Storage::makeDirectory('public/temp/small');
        Storage::makeDirectory('public/temp/medium');

        $imageName = uniqid('img_');
        $image = $request->file('image');

        $image_resize = InterventionImage::make($image->getRealPath());
        $image_resize->fit(300);
        $image_resize->save(public_path('storage/temp/small/'.$imageName.'.jpg'));

        $image_medium = InterventionImage::make($image->getRealPath());
        $image_medium->fit(1400, 900,  function ($constraint) {$constraint->upsize();});
        $image_medium->save(public_path('storage/temp/medium/'.$imageName.'.jpg'));

        $request->file('image')->storeAs('public/temp/original/', $imageName.'.jpg');


        return($imageName);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Zahra\Images  $images
     * @return \Illuminate\Http\Response
     */
    public function show(Image $images)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Zahra\Images  $images
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $images)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Zahra\Images  $images
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $images)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Zahra\Images  $images
     * @return \Illuminate\Http\Response
     */
    public function destroy($name)
    {
        // return ($name);
        Storage::delete('public/temp/small/'.$name.'.jpg');
        Storage::delete('public/temp/original/'.$name.'.jpg');
    }
}
