<?php

namespace Zahra\Http\Controllers;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Zahra\Category;
use Zahra\CategoryProject;
use Zahra\Image;
use Zahra\Project;

class ProjectController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Project $project)
    {
        if($request->ajax()){
            $projects = Project::orderBy('id', 'desc')->get();
            return response()->json($projects)->header('Vary', 'Accept');
        }

        $projects = Project::with('images','categories')->get();
        return view('backend.projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $categories = Category::all();
        return view ('backend.projects.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'categories' => 'required',
            'images' => 'required'
        ]);

        if ($validator->fails()) {
            if($request->categories){
                $activeCategories = Category::whereIn('id', $request->categories)->get();
                return redirect('project/create')
                        ->withErrors($validator)
                        ->withInput()->with('activeCategories', $activeCategories);
            }
            return redirect('project/create')
                        ->withErrors($validator)
                        ->withInput();
        }


        $project = new Project;

        $project->title = $request->title;
        $project->body = $request->body;
        $project->subtitle = $request->subtitle;
        $project->client_name = $request->client_name;
        $project->client_website = $request->client_website;
        $project->video_link = $request->video_link;
        $project->is_published = $request->is_published;
        $project->crop_images = (int)($request->crop_images === 'true');
        if($request->images){
            $project->main_image = $request->images[0];
        }


        $project->save();
        $lastId = $project->id;

        if($request->images){
            foreach($request->images as $image){
                Storage::move('public/temp/original/'.$image.'.jpg', 'public/images/'.$lastId.'/original/'.$image.'.jpg');
            }

            foreach($request->images as $image){
                Storage::move('public/temp/medium/'.$image.'.jpg', 'public/images/'.$lastId.'/medium/'.$image.'.jpg');
            }

            foreach($request->images as $image){
                Storage::move('public/temp/small/'.$image.'.jpg', 'public/images/'.$lastId.'/small/'.$image.'.jpg');
            }

            foreach ($request->images as $image) {
                $newImage = new Image;
                $newImage->project_id = $lastId;
                $newImage->large_image = $image;
                $newImage->small_image = $image;
                $newImage->save();
            }
        }

        foreach ($request->categories as $category) {
            $newCategory = new CategoryProject;
            $newCategory->category_id = $category;
            $newCategory->project_id = $lastId;
            $newCategory->save();
        }

        $isDeleted = Storage::deleteDirectory('public/temp');

        return redirect('project')->with('status', 'Project created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Zahra\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $projectId = $id;
        $project = Project::with('images', 'categories')->where('id', $id)->first();

        $categories = CategoryProject::where('project_id', $id)->pluck('category_id')->all();
        $projects = CategoryProject::whereIn('category_id', $categories)->get();
        $unique = $projects->unique('project_id');
        $unique = $unique->filter(function ($value, $key) use($projectId){
            return $value['project_id']!=$projectId;
        });
        $unique = $unique->unique('project_id')->pluck('project_id');

        $relatedProjects =  Project::with('images')->whereIn('id', $unique)->limit(3)->orderBy('id','desc')->get();

        return view('project', compact('project', 'relatedProjects'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Zahra\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Project $project)
    {
        $project = Project::with('images', 'categories')->where('id', $id)->first();

        $isDeleted = Storage::deleteDirectory('public/temp');
        $categories = Category::all();

        $file = new Filesystem();
        $file->copyDirectory('../storage/app/public/images/'.$id, '../storage/app/public/temp/');
        // return($project);

        return view ('backend.projects.edit', compact('categories', 'project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Zahra\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'categories' => 'required',
            'images' => 'required'
        ]);

        if ($validator->fails()) {
            if($request->categories){
                $activeCategories = Category::whereIn('id', $request->categories)->get();
                return redirect('project/edit/'.$id)
                        ->withErrors($validator)
                        ->withInput()->with('activeCategories', $activeCategories);
            }
            return redirect('project/edit/'.$id)
                        ->withErrors($validator)
                        ->withInput();
        }


        $project = new Project;
        $project = $project->find($id);
        $project->title = $request->title;
        $project->body = $request->body;
        $project->subtitle = $request->subtitle;
        $project->client_name = $request->client_name;
        $project->client_website = $request->client_website;
        $project->video_link = $request->video_link;
        $project->is_published = $request->is_published;
        $project->crop_images = (int)($request->crop_images === 'true');
        $project->save();

        $images = new Image;
        $images->where('project_id', $id)->delete();

        $categories = new CategoryProject;
        $categories->where('project_id', $id)->delete();

        Storage::deleteDirectory('public/images/'.$id);

        if($request->images){
            foreach($request->images as $image){
                Storage::move('public/temp/original/'.$image.'.jpg', 'public/images/'.$id.'/original/'.$image.'.jpg');
            }

            foreach($request->images as $image){
                Storage::move('public/temp/medium/'.$image.'.jpg', 'public/images/'.$id.'/medium/'.$image.'.jpg');
            }

            foreach($request->images as $image){
                Storage::move('public/temp/small/'.$image.'.jpg', 'public/images/'.$id.'/small/'.$image.'.jpg');
            }

            foreach ($request->images as $image) {
                $newImage = new Image;
                $newImage->project_id = $id;
                $newImage->large_image = $image;
                $newImage->small_image = $image;
                $newImage->save();
            }
        }

        if($request->categories){
           foreach ($request->categories as $category) {
                $newCategory = new CategoryProject;
                $newCategory->category_id = $category;
                $newCategory->project_id = $id;
                $newCategory->save();
            }
        }



        return redirect('project')->with('status', 'Project updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Zahra\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Project $project)
    {
        $project = $project->find($id);
        $project->delete();
        Storage::deleteDirectory('public/images/'.$id);
        return ('success');
    }

    public function toggleState($id, Project $project){
        $project = $project->find($id);
        $project->is_published = !$project->is_published;
        $project->save();
        return ($project->orderBy('id','desc')->get());
    }

    public function toggleFeaturedState($id, Project $project){
        $project = $project->find($id);
        $project->is_featured = !$project->is_featured;
        $project->save();
        return ($project->orderBy('id','desc')->get());
    }

}
