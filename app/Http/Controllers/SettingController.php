<?php

namespace Zahra\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as InterventionImage;
use Zahra\Setting;

class SettingController extends Controller
{
    public function index () {
        $setting = Setting::first();
        return view('backend.settings.index', compact('setting'));
    }

    /////////////////////Edit Social Link///////////////////////

    public function edit_social_link () {
        $setting = Setting::first();
        return view('backend.settings.social_link_edit', compact('setting'));
    }

    public function update_social_link (Request $request) {
        $setting = Setting::first();
        $setting->facebook_link = $request->facebook;
        $setting->twitter_link = $request->twitter;
        $setting->youtube_link = $request->youtube;
        $setting->googleplus_link = $request->google;
        $setting->pinterest_link = $request->pinterest;
        $setting->vimeo_link = $request->vimeo;
        $setting->linkedin_link = $request->linkedin;
        $setting->rssfeed_link = $request->rssfeed;
        $setting->save();

        return redirect('/setting')->with('status', 'Social links updated');
    }

    public function edit_site_settings () {
        $setting = Setting::first();
        return view('backend.settings.site_settings_edit', compact('setting'));
    }

    public function update_site_settings (Request $request) {
        $setting = Setting::first();
        $setting->site_title = $request->title;
        $setting->company = $request->address;
        $setting->map = $request->map;
        $setting->copyright = $request->copyright;
        $setting->save();

        return redirect('/setting')->with('site_settings_status', 'Site settings updated!');
    }

    public function edit_logo () {
        return view('backend.settings.logo_edit');
    }

    public function store_logo (Request $request) {
        // dd($request);
        if($request->hasFile('logo')){
            Storage::deleteDirectory('public/logo');
            Storage::makeDirectory('public/logo');
            $imageName = uniqid('img_');
            $image = $request->file('logo');
            $image_resize = InterventionImage::make($image->getRealPath());
            $image_resize->fit(92, 24);
            $image_resize->save(public_path('storage/logo/'.$imageName.'.png'));

            $setting = Setting::first();
            $setting->site_logo = $imageName;
            $setting->save();

            return redirect('/setting')->with('logo_status', 'Site logo has been changed');
        }

        return back()->with('logo_status', 'Something went wrong. Nothing has been changed!');

    }
}
