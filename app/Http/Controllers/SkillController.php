<?php

namespace Zahra\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Zahra\Skill;

class SkillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $skills = Skill::get();

        return view('backend.skills.index', compact('skills'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.skills.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Auth::id();
        Skill::create(['name' => $request->name, 'level' => $request->level, 'profile_id' => $id]);
        return redirect('/profile/skills')->with('status', 'New skill added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Zahra\Skill  $skill
     * @return \Illuminate\Http\Response
     */
    public function show(Skill $skills)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Zahra\Skill  $skill
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $skill = Skill::find($id);
        return view('backend.skills.edit', compact('skill'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Zahra\Skill  $skill
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $skill = Skill::find($id);
        $skill->name = $request->name;
        $skill->level = $request->level;
        $skill->save();
        return redirect('/profile/skills')->with('status', 'Skill updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Zahra\Skill  $skill
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $skill = Skill::find($id);
        $skill->delete();
        return back()->with('deleted', 'Skill removed!');
    }
}
