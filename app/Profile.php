<?php

namespace Zahra;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'name', 'email', 'user_id',
    ];

    public function skills(){
        return $this->hasMany('Zahra\Skill');
    }
}
