<?php

namespace Zahra;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function images(){
        return $this->hasMany('Zahra\Image');
    }

    public function categories(){
        return $this->belongsToMany('Zahra\Category');
    }

    public function has_categories(){
        return $this->hasMany('Zahra\CategoryProject');
    }
}
