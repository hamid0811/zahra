<?php

namespace Zahra\Providers;

use Illuminate\Support\ServiceProvider;
use Zahra\Project;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Project::deleting(function ($project){
            $project->images()->delete();
            $project->has_categories()->delete();
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
