<?php

namespace Zahra;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $fillable = [
        'name', 'level', 'profile_id',
    ];
}
