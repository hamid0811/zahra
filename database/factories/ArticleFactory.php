<?php

use Faker\Generator as Faker;

$factory->define(Zahra\Article::class, function (Faker $faker) {
    return [
        'title' => $faker->text(50),
        'body'  => $faker->text(200),
        'category' => $faker->randomElement(['movie', 'identity', 'web-design', 'graphic', 'logo']),
        'image' => $faker->randomElement(['https://placeimg.com/600/600/any/'.$faker->unique()->numberBetween($min = 1, $max = 100)])
    ];
});
