<?php

use Faker\Generator as Faker;

$factory->define(Zahra\Category::class, function (Faker $faker) {
    return [
        'name' => $faker->word
    ];
});
