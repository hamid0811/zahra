<?php

use Faker\Generator as Faker;

$factory->define(Zahra\CategoryProject::class, function (Faker $faker) {
    return [
        'project_id' => $faker->randomElement(Zahra\Project::pluck('id')->toArray()),
        'category_id' => $faker->randomElement(Zahra\Category::pluck('id')->toArray()),
    ];
});
