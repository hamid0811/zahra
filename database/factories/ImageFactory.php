<?php

use Faker\Generator as Faker;


$factory->define(Zahra\Image::class, function (Faker $faker) {
    return [
        'project_id' => $faker->randomElement(Zahra\Project::pluck('id')->toArray()),
        'small_image' => $faker->randomElement(['https://placeimg.com/600/600/any/'.$faker->unique()->numberBetween($min = 1, $max = 1000)]),
        'large_image' => $faker->randomElement(['https://placeimg.com/1400/900/any/'.$faker->unique()->numberBetween($min = 1, $max = 1000)])
    ];
});
