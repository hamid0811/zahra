<?php

use Faker\Generator as Faker;

$factory->define(Zahra\Project::class, function (Faker $faker) {
    return [
        'title' => $faker->text(50),
        'subtitle' =>$faker->text(30),
        'body'  => $faker->text(200),
        'is_published' => $faker->randomElement([1,0]),
        'is_featured' => $faker->randomElement([1,0]),
        'crop_images' => $faker->randomElement([1,0]),
        'main_image_id' => $faker->unique()->numberBetween($min = 1, $max = 100)
    ];
});
