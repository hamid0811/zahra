<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('site_title')->nullable();
            $table->string('company')->nullable();
            $table->string('map')->nullable();
            $table->string('copyright')->nullable();
            $table->string('facebook_link')->nullable();
            $table->string('twitter_link')->nullable();
            $table->string('googleplus_link')->nullable();
            $table->string('youtube_link')->nullable();
            $table->string('pinterest_link')->nullable();
            $table->string('vimeo_link')->nullable();
            $table->string('linkedin_link')->nullable();
            $table->string('rssfeed_link')->nullable();
            $table->string('site_logo')->nullable();
            $table->timestamps();
        });

        DB::table('settings')->insert(
            array(
                'copyright' => 'Copyright 2018 All Rights Reserved',
                'site_logo' => 'logo'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
