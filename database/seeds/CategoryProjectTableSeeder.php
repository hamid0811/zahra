<?php

use Illuminate\Database\Seeder;

class CategoryProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Zahra\CategoryProject::class, 60)->create();
    }
}
