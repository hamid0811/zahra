<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoryTableSeeder::class);
        $this->call(ArticlesTableSeeder::class);
        // $this->call(ProjectTableSeeder::class);
        $this->call(ImageTableSeeder::class);
        $this->call(CategoryProjectTableSeeder::class);
    }
}
