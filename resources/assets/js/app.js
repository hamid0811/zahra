/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');


import ToggleButton from 'vue-js-toggle-button';
Vue.use(ToggleButton);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('passport-clients',require('./components/passport/Clients.vue'));
Vue.component('passport-authorized-clients',require('./components/passport/AuthorizedClients.vue'));
Vue.component('passport-personal-access-tokens',require('./components/passport/PersonalAccessTokens.vue'));



Vue.component('upload-project-image', require('./components/UploadImage.vue'));
Vue.component('publish-project', require('./components/PublishProject.vue'));
Vue.component('categories', require('./components/Categories.vue'));
Vue.component('skills', require('./components/profile/Skills.vue'));

Vue.component('display-categories', require('./components/categories/DisplayCategories.vue'));

Vue.directive('tooltip', function(el, binding){
    $(el).tooltip({
             title: binding.value,
             placement: binding.arg,
             trigger: 'hover'
         })
})

const app = new Vue({
    el: '#vue-app',
});
