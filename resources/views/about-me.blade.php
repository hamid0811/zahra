<div class="portfolio-content">
    <div class="cbp-l-project-title">{{$profile->name}}</div>
    <div class="cbp-l-project-subtitle text-uppercase">{{$profile->subtitle}}</div>
    <img src="/storage/profile/{{$profile->photo}}.jpg" class="center-block img-rounded" alt="{{$profile->name}}" width="180" />
    <!--Container-->
    <div class="cbp-l-project-container">
        <!--Description-->
        <div class="cbp-l-project-desc">
            <div class="cbp-l-project-desc-title">
                <span>Biography</span>
            </div>
            <div class="cbp-l-project-desc-text"><?php echo $profile->biography ?></div>
        </div>
        <!--/Description-->
        <!--Project Details-->
        <div class="cbp-l-project-details">
            <div class="cbp-l-project-details-title">
                <span>I AM</span>
            </div>
            <!--List-->
            <ul class="cbp-l-project-details-list">
                @if($profile->name)
                    <li><strong>Name</strong>{{$profile->name}}</li>
                @endif
                @if($profile->dob)
                    <li><strong>Date of birth</strong>{{$profile->dob}}</li>
                @endif
                @if($profile->address)
                    <li><strong>Address</strong>{{$profile->address}}</li>
                @endif
                @if($profile->email)
                    <li><strong>Email</strong>{{$profile->email}}</li>
                @endif
                @if($profile->phone)
                    <li><strong>Phone</strong>{{$profile->phone}}</li>
                @endif
                @if($profile->skype)
                    <li><strong>Skype</strong>{{$profile->skype}}</li>
                @endif
            </ul>
            <!--/List-->
        </div>
        <!--/Project Details-->
    </div>
    <!--/Container-->
    <!--Container 2-->
    @if(count($profile->skills) > 0)
    <div class="cbp-l-project-container">
            <div class="cbp-l-project-desc-title">
                <span>Professional Skills</span>
            </div>
            <!--Skills-->
            <div class="cbp-l-project-desc-text">
                @foreach($profile->skills as $skill)
                    <span class="text-uppercase">{{$skill->name}}</span>
                    <div class="progress pror-bor-gey">
                      <div class="progress-bar progress-bar-gey" role="progressbar" aria-valuenow="{{$skill->level}}0" aria-valuemin="0" aria-valuemax="100" style="width: {{$skill->level}}0%"></div>
                    </div>
                @endforeach
            </div>
            <!--/Skills-->
    </div>
    @endif
    <!--/Container 2-->
    <!--Container 3-->
    @if($profile->experience)
    <div class="cbp-l-project-container">
        <!--Title-->
        <div class="cbp-l-project-desc-title">
            <span>EXPERIENCE</span>
        </div>
        <!--/Title-->
        <!--Text-->
        <div class="cbp-l-project-desc-text">
            <?php echo $profile->experience ?>
        </div>
        <!--/Text-->
    </div>
    @endif
    <!--/Container 3-->
    <br><br><br>
</div>
