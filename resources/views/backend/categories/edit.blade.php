@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Edit category</div>
                    <div class="card-body">
                        <form method="post" action="/category/update/{{$category->id}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="usr">Name:</label>
                                <input type="text" class="form-control" name="name" id="category" value="{{$category->name}}">
                            </div>
                          <button type="submit" class="btn btn-primary">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
