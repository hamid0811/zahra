@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Categories</div>

                <div class="card-body" id="vue-app">

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (session('deleted'))
                        <div class="alert alert-danger">
                            {{ session('deleted') }}
                        </div>
                    @endif

                    <table  class="table">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Projects</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    <td>
                                        {{ $category->name }}
                                    </td>
                                    <td>{{ $category->number_of_projects_count }}</td>
                                    <td>
                                        <div class="float-right">
                                            <div class="btn-group" role="group">
                                              <a class="btn btn-light" href="/category/edit/{{ $category->id }}">
                                                  <i class="material-icons">edit</i>
                                              </a>
                                              <button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="sr-only">Toggle Dropdown</span>
                                              </button>
                                              <div class="dropdown-menu">
                                                <form method="POST" action="/category/delete/{{$category->id}}">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="dropdown-item btn btn-light">Delete</button>
                                                </form>
                                              </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>

                <div class="card-footer">
                    <a href="/category/create" class="btn btn-primary">New Category</a>
                </div>
            </div>
        </div>
    </div>


</div>
@endsection
