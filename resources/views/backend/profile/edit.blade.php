@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Edit prfile</div>
                    <div class="card-body">
                        <form method="post" action="/profile/update/" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="usr">Name:</label>
                                <input type="text" class="form-control" name="name" value="{{$user->name}}">
                            </div>
                            <div class="form-group">
                                <label for="usr">Subtitle:</label>
                                <input type="text" class="form-control" name="subtitle" value="{{$user->subtitle}}">
                            </div>
                            <div class="form-group">
                                <label for="usr">Email:</label>
                                <input type="text" class="form-control" name="email" value="{{$user->email}}">
                            </div>
                            <div class="form-group">
                                <label for="usr">Date of birth:</label>
                                <input type="date" class="form-control" name="dob" value="{{$user->dob}}">
                            </div>
                            <div class="form-group">
                                <label for="usr">Address:</label>
                                <input type="text" class="form-control" name="address" value="{{$user->address}}">
                            </div>
                            <div class="form-group">
                                <label for="usr">Phone:</label>
                                <input type="text" class="form-control" name="phone" value="{{$user->phone}}">
                            </div>
                            <div class="form-group">
                                <label for="usr">Skype:</label>
                                <input type="text" class="form-control" name="skype" value="{{$user->skype}}">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Prfile picture:</label>
                                <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Biography:</label>
                                <textarea name="biography" class="form-control" id="textarea" rows="5">{{$user->biography}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Experience:</label>
                                <textarea name="experience" class="form-control" id="textarea" rows="5">{{$user->experience}}</textarea>
                            </div>
                            <a href="/profile" class="btn btn-danger">Cancel</a>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
