@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Profile</div>

                <div class="card-body" id="vue-app">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-8">
                            <p><span class="font-weight-bold">Name:</span> {{$profile->name}}</p>
                            <p><span class="font-weight-bold">Subtitle:</span> {{$profile->subtitle}}</p>
                            <p><span class="font-weight-bold">Email:</span> {{$profile->email}}</p>
                            <p><span class="font-weight-bold">Date of birth:</span> {{$profile->dob}}</p>
                            <p><span class="font-weight-bold">Address:</span> {{$profile->address}}</p>
                            <p><span class="font-weight-bold">Phone number:</span> {{$profile->phone}}</p>
                            <p><span class="font-weight-bold">Skype:</span> {{$profile->skype}}</p>
                            <p><span class="font-weight-bold">Biography:</span></p>
                            <?php echo $profile->biography ?>
                            <p><span class="font-weight-bold">Experience:</span></p>
                            <?php echo $profile->experience ?>
                        </div>
                        <div class="col-4">
                            <img class="img-fluid" src="/storage/profile/{{$profile->photo}}.jpg">
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <a href="/profile/edit" class="btn btn-primary">Edit Profile</a>
                </div>
            </div>
        </div>
    </div>


</div>
@endsection
