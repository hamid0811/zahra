@extends('layouts.app')

@section('content')



<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Create a new project</div>

                <div class="card-body">
                    @include ('layouts.errors')

                    <form method="post" action="/project/store">
                        {{ csrf_field() }}
                      <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" value="{{ old('title') }}" class="form-control" name="title" id="title">
                      </div>
                      <div class="form-group">
                        <label for="title">Subtitle</label>
                        <input type="text" class="form-control" name="subtitle" id="subtitle">
                      </div>
                      <div class="form-group">
                        <label for="client_name">Client name (Optional)</label>
                        <input type="text" class="form-control" name="client_name">
                      </div>
                      <div class="form-group">
                        <label for="client_website">Client website (Optional)</label>
                        <input type="text" class="form-control" name="client_website">
                      </div>
                      <div class="form-group">
                        <label for="video_link">Video link:</label>
                        <input type="text" class="form-control" name="video_link">
                      </div>
                      <div id="vue-app">
                        <upload-project-image :old_crop_images="'{{ json_encode(old('crop_images')) }}'" :crop_images="'undefined'" :old_images="'{{ json_encode(old('images')) }}'" ></upload-project-image>
                        <categories :categories="{{$categories}}" :old_active_categories="{{ json_encode(session('activeCategories')) }}" :active_categories="[]"></categories>
                      </div>
                      <div class="form-group">
                        <label for="body">Description</label>
                        <textarea name="body" value="hello" class="form-control" id="textarea" rows="5">{{ old('body') }}</textarea>
                      </div>
                      <button class="btn btn-default" name="is_published" value="0">Save</button>
                      <button class="btn btn-primary" name="is_published" value="1">Save & Publish</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
