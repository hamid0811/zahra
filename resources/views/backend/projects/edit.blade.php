@extends('layouts.app')

@section('content')



<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Edit project</div>

                <div class="card-body">
                    @include ('layouts.errors')

                    <form method="post" action="/project/patch/{{$project->id}}">
                        {{ csrf_field() }}
                      <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" name="title" id="title" value="{{$project->title}}" required>
                      </div>
                      <div class="form-group">
                        <label for="subtitle">Subtitle</label>
                        <input type="text" class="form-control" name="subtitle" id="subtitle" value="{{$project->subtitle}}">
                      </div>
                      <div class="form-group">
                        <label for="client_name">Client name (Optional)</label>
                        <input type="text" class="form-control" name="client_name" value="{{$project->client_name}}">
                      </div>
                      <div class="form-group">
                        <label for="client_website">Client website (Optional)</label>
                        <input type="text" class="form-control" name="client_website" value="{{$project->client_website}}">
                      </div>
                      <div class="form-group">
                        <label for="video_link">Video link:</label>
                        <input type="text" class="form-control" name="video_link" value="{{$project->video_link}}">
                      </div>
                      <div id="vue-app">
                          <upload-project-image :old_crop_images="'{{ json_encode(null) }}'" :images="{{$project->images}}" :crop_images="{{$project->crop_images}}"></upload-project-image>
                          <hr>
                          <categories :categories="{{$categories}}" :old_active_categories="{{ json_encode(session('activeCategories')) }}" :active_categories="{{$project->categories}}"></categories>
                          <hr>
                      </div>
                      <div class="form-group">
                        <label for="exampleFormControlTextarea1">Description</label>
                        <textarea name="body" class="form-control" id="textarea" rows="10">{{$project->body}}</textarea>
                      </div>
                      <button class="btn btn-default" name="is_published" value="0">Save</button>
                      <button class="btn btn-primary" name="is_published" value="1">Save & Publish</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
