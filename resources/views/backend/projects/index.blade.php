@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Projects</div>

                <div class="card-body" id="vue-app">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <publish-project></publish-project>
                </div>
                <div class="card-footer">
                    <a href="/project/create" class="btn btn-primary">New Project</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
