@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Site logo</div>

                <div class="card-body" id="vue-app">

                    @if (session('logo_status'))
                        <div class="alert alert-success">
                            {{ session('logo_status') }}
                        </div>
                    @endif

                    <img class="img-fluid" src="/storage/logo/{{$setting->site_logo}}.png">

                </div>
                <div class="card-footer">
                    <a href="/setting/logo/edit" class="btn btn-primary">Edit</a>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Site Settings</div>

                <div class="card-body" id="vue-app">

                    @if (session('site_settings_status'))
                        <div class="alert alert-success">
                            {{ session('site_settings_status') }}
                        </div>
                    @endif
                    <table  class="table">
                        <tbody>
                                <tr>
                                    <td>
                                        Site Title
                                    </td>
                                    <td>{{ $setting->site_title }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        Company Address
                                    </td>
                                    <td>{{ $setting->company }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        Map
                                    </td>
                                    <td>{{ $setting->map }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        Copyright
                                    </td>
                                    <td>{{ $setting->copyright}}</td>
                                </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <a href="/setting/site_settings/edit" class="btn btn-primary">Edit</a>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Social Links</div>

                <div class="card-body" id="vue-app">

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table  class="table">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Link</th>
                          </tr>
                        </thead>
                        <tbody>
                                <tr>
                                    <td>
                                        Facebook
                                    </td>
                                    <td>{{ $setting->facebook_link }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        Twitter
                                    </td>
                                    <td>{{ $setting->twitter_link }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        Google+
                                    </td>
                                    <td>{{ $setting->googleplus_link}}</td>
                                </tr>
                                <tr>
                                    <td>
                                        YouTube
                                    </td>
                                    <td>{{ $setting->youtube_link }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        Pinterest
                                    </td>
                                    <td>{{ $setting->pinterest_link }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        Vimeo
                                    </td>
                                    <td>{{ $setting->vimeo_link }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        LinkedIn
                                    </td>
                                    <td>{{ $setting->linkedin_link }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        rssfeed_link
                                    </td>
                                    <td>{{ $setting->rssfeed_link }}</td>
                                </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <a href="/setting/social_link/edit" class="btn btn-primary">Edit</a>
                </div>
            </div>
        </div>
    </div>


</div>
@endsection
