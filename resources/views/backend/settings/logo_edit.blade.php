@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Change logo</div>
                    <div class="card-body">
                        @if (session('logo_status'))
                            <div class="alert alert-danger">
                                {{ session('logo_status') }}
                            </div>
                        @endif
                        <form method="post" action="/setting/logo/" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Choose a logo:</label>
                                <input type="file" name="logo" class="form-control-file" id="exampleFormControlFile1">
                            </div>
                          <button type="submit" class="btn btn-primary">Upload</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
