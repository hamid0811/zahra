@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Edit site settings</div>
                    <div class="card-body">
                        <form method="post" action="/setting/site_settings/update/">
                            {{csrf_field()}}
                            @method('patch')
                            <div class="form-group">
                                <label for="title">Site title:</label>
                                <input type="text" class="form-control" name="title" value="{{$setting->site_title}}">
                            </div>
                            <div class="form-group">
                                <label for="address">Company Address:</label>
                                <input type="text" class="form-control" name="address" value="{{$setting->company}}">
                            </div>
                            <div class="form-group">
                                <label for="map">Map (Location ID):</label>
                                <input type="text" class="form-control" name="map" value="{{$setting->map}}">
                                <a href="https://developers.google.com/maps/documentation/javascript/examples/places-placeid-finder" target="_blank">Find a location's id</a>
                            </div>
                            <div class="form-group">
                                <label for="copyright">Copyright:</label>
                                <input type="text" class="form-control" name="copyright" value="{{$setting->copyright}}">
                            </div>
                          <button type="submit" class="btn btn-primary">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
