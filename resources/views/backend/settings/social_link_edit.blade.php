@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Edit social links</div>
                    <div class="card-body">
                        <div class="alert alert-info" role="alert">
                            <p>You need to enter the full path for each link.</p>
                            <p>Example: https://www.facebook.com/people/Zahra-Hussain/100016936306742</p>
                        </div>
                        <form method="post" action="/setting/social_link/update/">
                            {{csrf_field()}}
                            @method('patch')
                            <div class="form-group">
                                <label for="usr">Facebook:</label>
                                <input type="text" class="form-control" name="facebook" value="{{$setting->facebook_link}}">
                            </div>
                            <div class="form-group">
                                <label for="usr">Twitter:</label>
                                <input type="text" class="form-control" name="twitter" value="{{$setting->twitter_link}}">
                            </div>
                            <div class="form-group">
                                <label for="usr">Google+:</label>
                                <input type="text" class="form-control" name="google" value="{{$setting->googleplus_link}}">
                            </div>
                            <div class="form-group">
                                <label for="usr">YouTube:</label>
                                <input type="text" class="form-control" name="youtube"value="{{$setting->youtube_link}}">
                            </div>
                            <div class="form-group">
                                <label for="usr">Pinterest:</label>
                                <input type="text" class="form-control" name="pinterest" value="{{$setting->pinterest_link}}">
                            </div>
                            <div class="form-group">
                                <label for="usr">Vimeo:</label>
                                <input type="text" class="form-control" name="vimeo" value="{{$setting->vimeo_link}}">
                            </div>
                            <div class="form-group">
                                <label for="usr">LinkedIn:</label>
                                <input type="text" class="form-control" name="linkedin" value="{{$setting->linkedin_link}}">
                            </div>
                            <div class="form-group">
                                <label for="usr">RSS Feed:</label>
                                <input type="text" class="form-control" name="rssfeed" value="{{$setting->rssfeed_link}}">
                            </div>
                          <button type="submit" class="btn btn-primary">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
