@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Edit skill</div>
                    <div class="card-body">
                        <form method="post" action="/profile/skills/update/{{$skill->id}}">
                            {{csrf_field()}}
                            @method('PATCH')
                            <div class="row">
                                <div class="col-8">
                                    <div class="form-group">
                                        <label for="usr">Name:</label>
                                        <input type="text" class="form-control" name="name" value="{{ $skill->name }}">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Level:</label>
                                        <select name="level" class="form-control">
                                            <option disabled selected value="Skill Level">Skill Level</option>
                                            @for ($i = 1; $i <= 10; $i++)
                                                @if($i == $skill->level)
                                                <option selected>{{ $skill->level }}</option>
                                                @endif
                                                <option>{{ $i }}</option>
                                            @endfor
                                        </select>

                                    </div>
                                </div>
                            </div>


                          <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
