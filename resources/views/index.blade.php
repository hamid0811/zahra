<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{$settings->site_title}}</title>
        <meta name="description" content="Neuer is a responsive portfolio template for creative people.">
        <meta name="author" content="ThemeBang">
        <meta name="designer" content="ThemeBang">
        <meta name="reply-to" content="themebang@gmail.com">
        <meta name="copyright" content="(c) 2017 ThemeBang - www.themebang.com">
        <meta name="robots" content="index, follow">
        <!--Verifitication-->
        <meta name="google-site-verification" content="">
        <meta name="alexaVerifyID" content="">
        <meta name="yandex-verification" content="">
        <meta name="msvalidate.01" content="">
        <!-- / Verifitication-->
        <link rel="shortcut icon" href="contents/images/icons/favicon.png">
        <link rel="canonical" href="http://www.domainname.com/">
        <!-- CSS -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300i,700" rel="stylesheet" type="text/css">
        <link href="contents/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="contents/css/hover-min.css" rel="stylesheet" type="text/css">
        <link href="contents/portfolio/css/cubeportfolio.css" rel="stylesheet" type="text/css">
        <link href="contents/portfolio/css/portfolio.min.css" rel="stylesheet" type="text/css">
        <link href="contents/css/customize.css" rel="stylesheet" type="text/css">
        <!-- Font Awesome -->
        <script src="https://use.fontawesome.com/e9965a82bb.js"></script>
    </head>
    <body>
        <div class="container-fluid">
          <!--Menu-->
          <div class="d-none d-md-block menu">
            <div class="list-group cbp-l-filters-button js-filters-juicy-projects">
                <div data-filter="*" class="cbp-filter-item list-group-item logo">
                    <img src="/storage/logo/{{$settings->site_logo}}.png" alt="logo"/>
                </div>
                <div id="menu-links">
                    <a href="/profile/about-me" rel="nofollow" class="cbp-singlePage list-group-item hvr-underline-from-left text-uppercase" data-cbp-singlePage="about-me">About Me</a>
                    <div data-filter="*" class="cbp-filter-item list-group-item hvr-underline-from-left text-uppercase"> All Portfolio </div>
                    @foreach($categories as $category)
                        <div data-filter=".{{$category->id}}" class="cbp-filter-item list-group-item hvr-underline-from-left text-uppercase"> {{$category->name}} </div>
                    @endforeach
                </div>
                <!--Contact-->
                <div class="list-group-item">
                    <address>
                        @if($settings->company)
                            <strong>Company, Inc.</strong><br>
                            {{ $settings->company }}
                            <br>
                            <br /><br />
                        @endif
                        @if($settings->map)
                            <a href="#map" data-toggle="modal"><i class="fa fa-map-o" aria-hidden="true"></i></a>
                        @endif
                    </address>
                    <address>
                        <strong>{{$contact->name}}</strong><br>
                        <a href="mailto:#">{{$contact->email}}</a>
                    </address>
                </div>
                <!--/Contact-->
            </div>
          </div>
          <!-- /Menu-->
          <div class="main">
            <!--Mobile Menu-->
            <div class="d-block d-md-none">
                <nav class="navbar navbar-light">
                  <div class="container-fluid">
                    <div id="BtnMobilMenu" class="text-center">
                      <img data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" src="/storage/logo/{{$settings->site_logo}}.png" alt="img2"/>
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" aria-controls="bs-example-navbar-collapse-1" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                      </button>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="navbar-nav js-filters-juicy-projects">
                        <li class="cbp-singlePage list-group-item text-center hvr-underline-from-center text-uppercase"><a href="/profile/about-me" class="cbp-singlePage" data-cbp-singlePage="myCustomSinglePage1">About Me</a></li>
                        <li data-filter="*" class="cbp-filter-item list-group-item text-center hvr-underline-from-center text-uppercase"> All Portfolio</li>
                        @foreach($categories as $category)
                            <div data-filter=".{{$category->id}}" class="cbp-filter-item list-group-item text-center hvr-underline-from-center text-uppercase"> {{$category->name}} </div>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </nav>
            </div>
            <!-- /Mobile Menu-->
            <!--portfolio -->
            <div class="portfolio-content portfolio-1">
                <!--portfolio Grid-->
                <div id="js-grid-juicy-projects" class="cbp">
                    @foreach ($projects as $project)

                    <div class="cbp-item

                        @foreach ($project->categories as $category)
                                    {{$category->id}}
                        @endforeach

                    ">
                        <div class="cbp-item-wrap">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img src="/storage/images/{{$project->id}}/small/{{$project->images[0]->small_image}}.jpg" alt="img3"> </div>
                                <div class="cbp-caption-activeWrap">
                                    <div class="cbp-l-caption-alignCenter">
                                        <div class="cbp-l-caption-body">
                                            <div class="btn-group">
                                                <a href="project/{{$project->id}}" class="cbp-singlePage btn" rel="nofollow" data-cbp-singlePage="projects">more info</a>
                                                @if($project->video_link)
                                                    <a href="{{$project->video_link}}" class="cbp-lightbox btn btn-sm btn-right" data-title="{{$project->title}}">view video</a>
                                                @else
                                                    <a href="/storage/images/{{$project->id}}/original/{{$project->images[0]->large_image}}.jpg" class="cbp-lightbox btn btn-sm btn-right">Vieo larger</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cbp-l-grid-projects-title uppercase text-center uppercase text-center">{{$project->title}}</div>
                            <div class="cbp-l-grid-projects-desc uppercase text-center uppercase text-center">
                                @foreach ($project->categories as $category)
                                    {{$category->name}}

                                    @if ($loop->last)
                                        @break
                                    @endif
                                     /
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <!-- /portfolio Grid-->
                <!--portfolio loadMore-->
                <div id="js-loadMore-juicy-projects" class="cbp-l-loadMore-button">
                    <a href="loadMorePortfolio" class="cbp-l-loadMore-link hvr-underline-from-center text-uppercase" rel="nofollow">
                        <span class="cbp-l-loadMore-defaultText">load more</span>
                        <span class="cbp-l-loadMore-loadingText">loading...</span>
                        <span class="cbp-l-loadMore-noMoreLoading">not load more</span>
                    </a>
                </div>
                <!-- /portfolio loadMore-->
            </div>
            <!-- /portfolio -->
          </div>
        </div>
        <!--footer -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="d-none d-md-block col-md-5 col-lg-5">
                        @if($settings->copyright)
                            <small class="footer-small">{{ $settings->copyright }}</small>
                        @endif
                    </div>
                    <div class="col-sm-12 col-md-7 col-lg-7">
                        <!--Address -->
                        <div class="d-block d-md-none text-center">
                            <address>
                                @if($settings->company)
                                    <strong>Company, Inc.</strong><br>
                                    {{ $settings->company }}
                                    <br>
                                    <br /><br />
                                @endif
                                @if($settings->map)
                                    <a href="#map" data-toggle="modal"><i class="fa fa-map-o" aria-hidden="true"></i></a>
                                @endif
                            </address>
                            <address>
                                <strong>{{$contact->name}}</strong><br>
                                <a href="mailto:#">{{$contact->email}}</a>
                            </address>
                        </div>
                        <!-- /Address -->
                        <!--Social Media -->
                            <ul class="social-icons">
                                @if($settings->facebook_link)
                                    <li class="facebook"><a href="{{$settings->facebook_link}}" target="_blank">Facebook</a> </li>
                                @endif
                                @if($settings->twitter_link)
                                    <li class="twitter"><a href="{{$settings->twitter_link}}" target="_blank">Twitter</a></li>
                                @endif
                                @if($settings->google_link)
                                    <li class="googleplus"> <a href="{{$settings->google_link}}" target="_blank">Google +</a></li>
                                @endif
                                @if($settings->youtube_link)
                                    <li class="youtube"><a href="{{$settings->youtube_link}}" target="_blank">YouTube</a></li>
                                @endif
                                @if($settings->pinterest_link)
                                    <li class="pinterest"> <a href="{{$settings->pinterest_link}}" target="_blank">Pinterest</a></li>
                                @endif
                                @if($settings->vimeo_link)
                                    <li class="vimeo"><a href="{{$settings->vimeo_link}}" target="_blank">Vimeo</a></li>
                                @endif
                                @if($settings->linkedin_link)
                                    <li class="linkedin"><a href="{{$settings->linkedin_link}}" target="_blank">LinkedIn</a></li>
                                @endif
                                @if($settings->rssfeed_link)
                                    <li class="rss"><a href="{{$settings->rssfeed_link}}" target="_blank">RSS Feed</a></li>
                                @endif
                            </ul>
                        <div class="clearfix"></div>
                        <!-- /Social Media -->
                    </div>
                </div>
            </div>
        </footer>
        <!-- /footer -->
        <!-- /.modal Google Maps -->
        <div id="map" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Company, Inc.</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div id="map-canvas" style="width:100%; height:450px">
                            <iframe src="https://www.google.com/maps/embed/v1/place?q=place_id:{{$settings->map}}&key=AIzaSyBad8Je938IWh7fiz3CeCzbChFrl6a81bo" height="450" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal Google Maps-->
        <!-- jQuery -->
        <script src="contents/js/jquery-3.2.1.min.js" type="text/javascript"></script>
        <script src="contents/js/popper.min.js" type="text/javascript"></script>
        <script src="contents/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="contents/portfolio/js/jquery.cubeportfolio.js" type="text/javascript"></script>
        <script src="contents/portfolio/js/portfolio-1.js" type="text/javascript"></script>
        <script src="contents/js/retina.min.js" type="text/javascript"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="contents/js/html5shiv.min.js"></script>
        <script src="contents/js/respond.min.js"></script>
        <![endif]-->
        <script src="contents/js/main.js" type="text/javascript"></script>
        <!--Google Analytics -->
        <!-- <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-24357544-3', 'auto');
          ga('send', 'pageview');

        </script> -->
        <!-- /Google Analytics -->

        <script>
            jQuery("#grid-container").cubeportfolio('append', '<div class="logo cbp-item">my awesome content to append to plugin</div> <div class="logo cbp-item">my second awesome content to append to plugin</div>');
        </script>
    </body>
</html>
