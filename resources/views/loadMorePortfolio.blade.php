<!--LoadMore First block-->
<div class="cbp-loadMore-block1">
    @foreach ($projects as $project)
        <div class="cbp-item @foreach ($project->categories as $category){{$category->id}}@endforeach">
            <div class="cbp-item-wrap">
                <div class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img src="/storage/images/{{$project->id}}/small/{{$project->images[0]->small_image}}.jpg" alt="img3"> </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignCenter">
                            <div class="cbp-l-caption-body">
                                <div class="btn-group">
                                    <a href="project/{{$project->id}}" class="cbp-singlePage btn" rel="nofollow" data-cbp-singlePage="projects">more info</a>
                                    @if($project->video_link)
                                        <a href="{{$project->video_link}}" class="cbp-lightbox btn btn-sm btn-right" data-title="{{$project->title}}">view video</a>
                                    @else
                                        <a href="/storage/images/{{$project->id}}/original/{{$project->images[0]->large_image}}.jpg" class="cbp-lightbox btn btn-sm btn-right">Vieo larger</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cbp-l-grid-projects-title uppercase text-center uppercase text-center">{{$project->title}}</div>
                <div class="cbp-l-grid-projects-desc uppercase text-center uppercase text-center">
                    @foreach ($project->categories as $category)
                        {{$category->name}}

                        @if ($loop->last)
                            @break
                        @endif
                         /
                    @endforeach
                </div>
            </div>
        </div>
    @endforeach
</div>
<!--/LoadMore second block-->
