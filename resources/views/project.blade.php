<div class="portfolio-content">
    <!--Title and Subtitle-->
    <div class="cbp-l-project-title">{{$project->title}}</div>
    <div class="cbp-l-project-subtitle">{{$project->subtitle}}</div>
    <!--/Title and Subtitle-->
    <!--Slider-->
    <div class="cbp-slider">
        <ul class="cbp-slider-wrap">
            @foreach($project->images as $image)
            <li class="cbp-slider-item">
                <a href="/storage/images/{{$project->id}}/original/{{$image->large_image}}.jpg" class="cbp-lightbox">
                    @if($project->crop_images == 1)
                        <img height="300" src="/storage/images/{{$project->id}}/medium/{{$image->large_image}}.jpg" alt="">
                    @endif

                    @if($project->crop_images == 0)
                        <img height="500" src="/storage/images/{{$project->id}}/original/{{$image->large_image}}.jpg" alt="">
                    @endif
                </a>
            </li>
            @endforeach
        </ul>
    </div>
    <!--/Slider-->
    <!--Container-->
    <div class="cbp-l-project-container">
        <!--Description-->
        <div class="cbp-l-project-desc">
            <div class="cbp-l-project-desc-title">
                <span>Project Description</span>
            </div>
            <div class="cbp-l-project-desc-text"><?php echo $project->body ?></div>
        </div>
        <!--/Description-->
        <!--/Detail List-->
        <div class="cbp-l-project-details">
            <div class="cbp-l-project-details-title">
                <span>Project Details</span>
            </div>
             <!-- List-->
            <ul class="cbp-l-project-details-list">
                @if($project->client_name)
                    <li><strong>Client</strong>{{$project->client_name}}</li>
                @endif
                <li><strong>Date</strong>{{ \Carbon\Carbon::parse($project->created_at)->format('d/m/Y')}}</li>
                <li><strong>Categories</strong>
                    @foreach ($project->categories as $category)
                        {{$category->name}}

                        @if ($loop->last)
                            @break
                        @endif
                         ,
                    @endforeach
                </li>
            </ul>
             <!--/List-->
             @if($project->client_website)
                <a href="{{$project->client_website}}" target="_blank" class="cbp-l-project-details-visit btn red uppercase">visit the site</a>
            @endif
        </div>
        <!--/Detail List-->
    </div>
    <!--/Container-->
    <!--Container 2-->
    <div class="cbp-l-project-container">
        <div class="cbp-l-project-related">
            <!--Title-->
            @if(!$relatedProjects->isEmpty())
            <div class="cbp-l-project-desc-title">
                <span>Related Projects</span>
            </div>
            @endif
            <!--/Title-->
            <!--Related List-->
            @if(!$relatedProjects->isEmpty())
                <ul class="cbp-l-project-related-wrap">
                    @foreach($relatedProjects as $relatedProject)
                        <li class="cbp-l-project-related-item">
                            <a href="project/{{$relatedProject->id}}" class="cbp-singlePage cbp-l-project-related-link " rel="nofollow" data-cbp-singlePage="projects">
                                <img src="/storage/images/{{$relatedProject->id}}/small/{{$relatedProject->images[0]->small_image}}.jpg" alt="">
                                <div class="cbp-l-project-related-title">{{ $relatedProject->title }}</div>
                            </a>
                        </li>
                    @endforeach
                </ul>
            @endif
            <!--/Related List-->
        </div>
    </div>
    <!--/Container 2-->
    <br>
    <br>
    <br>
</div>
