<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('home', 'Api\HomeController@index');
Route::patch('/home/{id}', 'Api\HomeController@update')->middleware('auth:api');

Route::delete('/project/delete/{id}', 'Api\ProjectController@destroy')->middleware('auth:api');
