<?php

use App\Article;
use Zahra\Category;
use Zahra\Project;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/typography', function(){
    return view('typography');
});

Route::get('/', 'HomeController@index');


Route::get('/loadMorePortfolio ', function(){
    $categories = Category::get();
    $projects = Project::with('images','categories')->where(['is_featured' => '0', 'is_published' => '1'])->get();
    return view('loadMorePortfolio', compact('projects', 'categories'));
});



Route::post('/image/store', 'ImageController@store')->middleware('auth');
Route::delete('/image/delete/{name}', 'ImageController@destroy')->middleware('auth');

Route::prefix('project')->group(function (){
    Route::get('/', 'ProjectController@index')->middleware('auth')->name('projects');
    Route::post('patch/{id}', 'ProjectController@update')->middleware('auth');
    Route::get('create', 'ProjectController@create')->middleware('auth');
    Route::get('edit/{id}', 'ProjectController@edit')->middleware('auth');
    Route::post('store', 'ProjectController@store')->middleware('auth');
    Route::get('{id}', 'ProjectController@show');
    Route::delete('delete/{id}', 'ProjectController@destroy')->middleware('auth');
    Route::patch('toggleState/{id}', 'ProjectController@toggleState')->middleware('auth');
    Route::patch('toggleFeaturedState/{id}', 'ProjectController@toggleFeaturedState')->middleware('auth');
});


Auth::routes();

Route::prefix('admin')->group(function(){
    Route::get('/', 'ProjectController@index')->middleware('auth')->name('projects');
});

Route::prefix('category')->group(function(){
    Route::get('/', 'CategoryController@index')->middleware('auth');
    Route::get('/create', 'CategoryController@create')->middleware('auth');
    Route::get('edit/{id}', 'CategoryController@edit')->middleware('auth');
    Route::post('update/{id}', 'CategoryController@update')->middleware('auth');
    Route::post('store', 'CategoryController@store')->middleware('auth');
    Route::delete('delete/{id}', 'CategoryController@destroy')->middleware('auth');
});

Route::prefix('profile')->group(function () {
    Route::get('/', 'ProfileController@index')->middleware('auth');
    Route::get('/edit/', 'ProfileController@edit')->middleware('auth');
    Route::post('/update/', 'ProfileController@update')->middleware('auth');
    Route::get('/about-me', 'ProfileController@show');
    Route::get('/skills', 'SkillController@index')->middleware('auth');
    Route::get('/skills/create', 'SkillController@create')->middleware('auth');
    Route::post('/skills/store', 'SkillController@store')->middleware('auth');
    Route::delete('/skills/delete/{id}', 'SkillController@destroy')->middleware('auth');
    Route::get('/skills/edit/{id}', 'SkillController@edit')->middleware('auth');
    Route::patch('/skills/update/{id}', 'SkillController@update')->middleware('auth');
});

Route::prefix('setting')->group( function () {
    Route::get('/', 'SettingController@index')->middleware('auth');
    Route::patch('/social_link/update', 'SettingController@update_social_link')->middleware('auth');
    Route::get('/social_link/edit', 'SettingController@edit_social_link')->middleware('auth');
    Route::get('site_settings/edit', 'SettingController@edit_site_settings')->middleware('auth');
    Route::patch('site_settings/update', 'SettingController@update_site_settings')->middleware('auth');
    Route::post('/logo', 'SettingController@store_logo')->middleware('auth');
    Route::get('/logo/edit', 'SettingController@edit_logo')->middleware('auth');
});
